## Lang is a project of low level programming language

### Overwiew
The Lang project was started to improve my programming skills.
However it solve problem of switching from low level to high level
programming language and vice versa

A small convenience is that Lang base on BSD license,
so you can use it for own purpose, just mention me as author beforehand.
Your program can be not even open source.

