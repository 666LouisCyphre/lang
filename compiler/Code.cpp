#include <iostream>
#include "Code.h"

TextSection::TextSection(std::queue<std::string>& values, std::queue<std::string>& instructionList)
	:values(values), instructionList(instructionList) {}

void TextSection::standardFunction() {
#if 0
	"mov",
		"push",
		"pop",
		"cmp",
		"sub",
		"add",
		"div",
		"mul",
		"isub",
		"iadd",
		"idiv",
		"imul",
		"xor",
		"inc",
		"dec",
#endif
	std::cout << instructionList.front();
	while (!values.empty()) values.pop();
}

void TextSection::mov() {
	std::cout << values.front() << " ";
	values.pop();
	std::cout << values.front() << " ";
	values.pop();
	std::cout << values.front() << " ";
	values.pop();
}