#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <queue>

class Memory {
protected:
	std::string r1;
	std::string r2;
	std::string r3;
	std::string r4;
	std::string r5;
	std::string r6;
	std::string r7;
	std::string r8;
	std::string r9;
	std::string r10;
	std::string r11;
	std::string r12;
	std::string r13;
	std::string r14;
	std::string r15;
	std::string r16;
	std::vector<std::string>varNames;
	std::vector<std::string>varTypes;
	std::vector<std::string>varValues;
public:
	void clearRegister(std::string reg);
	void freeDynamicMemory(std::string cell);
	void cellSize(std::string cell);
};
