#pragma once

#define KEYWORD "KEYWORD"
#define LITERAL "LITERAL"
#define IDENTIFIER "IDENTIFIER"
#define LABEL "LABEL"
#define REGISTER "REGISTER"
#define SECTION "SECTION"
#define GLOBAL "GLOBAL"

#define VARIABLE_DECLARATION "VARIABLE_DECLARATION"
#define STANDARD_FUNCTION "STANDARD_FUNCTION"
#define SYSTEM_FUNCTION "SYSTEM_FUNCTION"
#define FUNCTION_DECLARATION "FUNCTION_DECLARATION"
#define MAIN_DECLARATION "MAIN_DECLARATION"
#define SECTION_DECLARATION "SECTION_DECLARATION"
#define RUN "RUN"
#define JUMP "JUMP"

const char delim[4] = { 32, 9, 44, 59 };
const std::string directiveKeywords[2] = { "section", "global" };
const std::string sectionTypes[2] = { "text", "data" };
const std::string varType[3] = { "dint", "dfloat", "dchar" };

const std::string instructionSet[28] {
	"mov",
	"push",
	"pop",
	"cmp",
	"sub",
	"add",
	"div",
	"mul",
	"isub",
	"iadd",
	"idiv",
	"imul",
	"xor",
	"call",
	"ret",
	"jmp",
	"jne",
	"jz",
	"jg",
	"je",
	"inc",
	"dec",
	"section",
	"global",
	"dchar",
	"dint",
	"dfloat",
	"run"
};

const std::string sysFunc[5]{
	"exit",
	"print"
};

const std::string registers[23]{
	"sys",
	"stdout",
	"stdfile",
	"stdin",
	"excode",
	"r1",
	"r2",
	"r3",
	"r4",
	"r5",
	"r6",
	"r7",
	"r8",
	"r9",
	"r10",
	"r11",
	"r12",
	"r13",
	"r14",
	"r15",
	"r16"
};