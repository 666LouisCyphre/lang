#pragma once
#include <iostream>
#include "instructionSet.h"
#include "file.h"
#include "Data.h"
#include "Code.h"

class Compiler {
private:
	void declareSection();
	std::queue<std::string>values;
	std::queue<std::string>instructionList;
	std::string exceptionThrownValue;
	int lineNumber;
	void catchExceptionMessage(std::string message);
	DataSection* var;
	TextSection* code;
protected:
	bool section;
public:
	Compiler();
	~Compiler();
	void run(std::string fileName);
	void loadDataSection();
	void loadTextSection();
	void loadOperations(std::string name);
	void recogniseInstructions();
	void Log();
};