#include <iostream>
#include "Data.h"

DataSection::DataSection(std::queue<std::string>& values, std::queue<std::string>& instructionList)
	:values(values), instructionList(instructionList) {

}

void DataSection::declareVariable() {
	std::string type = "";
	std::string name = "";
	std::string value = "";
	try {
		if (instructionList.front() != VARIABLE_DECLARATION) throw std::logic_error("Unexpected operation in data section.");
		instructionList.pop();
		varTypes.push_back(values.front());
		values.pop();
		varNames.push_back(values.front());
		values.pop();
		varValues.push_back(values.front());
		values.pop();
	}
	catch (std::logic_error& e) {
		std::cout << e.what() << " Expected variables declarations in data section\n";
	}
}