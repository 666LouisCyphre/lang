#pragma once
#include <iostream>
#include "Data.h"

class TextSection :public Memory {
private:
	std::queue<std::string>& values;
	std::queue<std::string>& instructionList;
	void mov();
public:
	TextSection(std::queue<std::string>& values, std::queue<std::string>& instructionList);
	void standardFunction();
	void systemFunction() {}
	void declareMain() {}
	void declareFunction() {}
	void run() {}
	void jump() {}
};