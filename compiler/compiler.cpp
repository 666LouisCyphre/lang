#include <iostream>
#include "compiler.h"

Compiler::Compiler() {
	var = new DataSection(values, instructionList);
	code = new TextSection(values, instructionList);
	lineNumber = 0;
}

Compiler::~Compiler() {
	delete var;
	delete code;
}

void Compiler::catchExceptionMessage(std::string message) {
	std::cout << "In line: " << lineNumber << " " << exceptionThrownValue << message << std::endl;
}

void Compiler::run(std::string fileName) {
	loadOperations(fileName);
	recogniseInstructions();
	declareSection();
}

void Compiler::declareSection() {
	std::string current = "";
	unsigned int iter = 0;
	try {
		++lineNumber;
		current = instructionList.front();
		instructionList.pop();
		if (current != SECTION_DECLARATION) throw std::logic_error("Instruction out of section.");
		for(iter; iter < 2; ++iter) {
			current = values.front();
			values.pop();
			if (current == "data") loadDataSection();
			else if (current == "text") loadTextSection();
			else throw std::invalid_argument("Invalid section name.");
		}
		if (values.size() != 0) throw std::logic_error("Multiple section declaration.");
	}
	catch (std::logic_error& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Expected code in 2 sections");
	}
	catch (std::invalid_argument& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Expected only data and text sections");
	}
}

void Compiler::loadDataSection() {
	std::string currentOperation = "";
	currentOperation = instructionList.front();
	while (currentOperation != SECTION_DECLARATION) {
		++lineNumber;
		var->declareVariable();
		currentOperation = instructionList.front();
	}
}

void Compiler::loadTextSection() {
	std::string current = "";
	instructionList.pop();
	while (!values.empty()) {
		current = instructionList.front();
		++lineNumber;
		if (current == STANDARD_FUNCTION) code->standardFunction();
		/*else if (current == SYSTEM_FUNCTION) code->systemFunction();
		else if (current == MAIN_DECLARATION) code->declareMain();
		else if (current == FUNCTION_DECLARATION) code->declareFunction();
		else if (current == RUN) code->run();
		else if (current == JUMP) code->jump();
		else std::cout << "Unexpected error in line: " << lineNumber << "\n";*/
	}
}

void Compiler::recogniseInstructions() {
	int i = 0, size = values.size();
	std::string current = "";
	char* ch;
	std::queue<std::string>temp;
	for (i; i < size; ++i) {
		current = values.front();
		values.pop();
		ch = &current[0];
		if (!islower(*ch) && !isdigit(*ch)) instructionList.push(current);
		else temp.push(current);
	}
	values = temp;
	temp.~queue();
}

void Compiler::loadOperations(std::string name) {
	File* file = new File(name);
	file->readFile(values);
	delete file;
}