#include <iostream>
#include "compiler.h"
#include <ctime>
#include <vld.h>

int main(void) {
	clock_t start, stop;
	double time = 0;
	start = clock();
	Compiler* compiler = new Compiler();
	compiler->run("tets.out");
	delete compiler;
	stop = clock();
	time = (double)(stop - start) / CLOCKS_PER_SEC;
	std::cout << "\nProcess done in: " << time << " seconds.\nPress enter to continue . . . ";
	(void)getchar();
	(void)getchar();
	return 0;
}