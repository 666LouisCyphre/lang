#pragma once
#include <iostream>
#include "Memory.h"
#include "instructionSet.h"

class DataSection :public Memory {
	std::queue<std::string>& values;
	std::queue<std::string>& instructionList;
public:
	DataSection(std::queue<std::string>& values, std::queue<std::string>& instructionList);
	void declareVariable();
	void assign();
	//void declareTable();
	void Log();
};