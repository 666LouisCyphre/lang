#include <iostream>
#include "analyze.h"

Analyze::Analyze(std::string name) {
	this->fileName = name;
}

Analyze::~Analyze() {
	//file1.~basic_fstream();
}

int Analyze::fileSize() {
	int rowsNumber = 0;
	std::string line;
	file1.open(fileName, std::ios::in);
	while (getline(file1, line)) ++rowsNumber;
	file1.close();
	return rowsNumber;
}

void Analyze::clearVectors() {
	token.clear();
	value.clear();
}

void Analyze::nameOutputFile() {
	outputFileName = fileName;
	outputFileName.erase(outputFileName.end() - 3, outputFileName.end());
	outputFileName.append("out");
}

void Analyze::toLower(std::string& str) {
	char* ch = &str[0];
	int i = 0, size = str.size();
	for (i; i < size; ++i) {
		if (isupper(*ch)) *ch = tolower(*ch);
		++ch;
	}
}

void Analyze::run() {
	nameOutputFile();
	File* file = new File(outputFileName);
	file->clearFile();
	delete file;
	Lex* lex;
	Parser* parser;
	file = new File(fileName);
	int i = 1, size = fileSize()+1;
	for (i; i < size; ++i) {
		file->readSpecificLine(i, line);
		toLower(line);
		lex = new Lex(line, token, value);
		lex->getLexem();
		lex->~Lex();

		parser = new Parser(token, value, i);
		parser->parse(outputFileName);
		parser->~Parser();
		clearVectors();
		delete lex;
		delete parser;
	}
	delete file;
}