#include <iostream>
#include "lexer.h"

Lex::Lex(std::string line, std::vector<std::string>& tok, std::vector<std::string>& val) 
	:token(tok), value(val) {
	this->line = line;
}

Lex::~Lex() {
	words.~queue();
	//line.~basic_string();
}

void Lex::split() {
	std::string word = "";
	int lineLen = line.length() + 1;
	int i = 0;
	char* ch = &line[0];
	for (i; i < lineLen; ++i) {
		if ((std::find(delim, std::end(delim), *ch) != std::end(delim) || *ch == '\0') && !word.empty()) {
			words.push(word);
			word.clear();
			if (*ch == 59) break;
			ch++;
		}
		else if (std::find(delim, std::end(delim), *ch) != std::end(delim)) ch++;
		else if (*ch == '\"') {
			ch++;
			while (*ch != '\"') {
				word += *ch;
				ch++;
			}
			i += word.length()+1;
			words.push(word);
			word.clear();
			ch++;
		}
		else {
			word += *ch;
			ch++;
		}
	}
}

void Lex::match() {
	std::string current = "";
	auto push = [this, &current](std::string lexem) {
		token.push_back(lexem);
		value.push_back(current);
	};
	do {
		current = words.front();
		words.pop();
		if (std::find(instructionSet, std::end(instructionSet), current) != std::end(instructionSet)) {
			push("KEYWORD");
		}
		else if ([](std::string cur) {return std::all_of(cur.begin(), cur.end(), ::isdigit); }(current)) {
			push("LITERAL");
		}
		else if (std::find(varType, std::end(varType), current) != std::end(varType)) {
			push("KEYWORD");
		}
		else if (std::find(registers, std::end(registers), current) != std::end(registers)) {
			push("REGISTER");
		}
		else if (!value.empty() && std::find(varType, std::end(varType), value.back()) != std::end(varType)) {
			push("IDENTIFIER");
		}
		else if (current[current.length() - 1] == 58) {
			push("LABEL");
		}
		else {
			push("LITERAL");
		}
	} while (!words.empty());
}

void Lex::getLexem() {
	if (!line.empty()) {
		split();
		match();
	}
}