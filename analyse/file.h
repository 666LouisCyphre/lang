#pragma once
#include <iostream>
#include <queue>
#include <fstream>
#include <string>

class File {
private:
	std::fstream file1;
	std::string name;
public:
	File(std::string name);
	~File();
	void saveToFile(std::queue<std::string>& operationList);
	void readFile(std::queue<std::string>& operationList);
	void printFile();
	void clearFile();
	void readSpecificLine(int lineNumber, std::string& line);
};