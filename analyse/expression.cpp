#include <iostream>
#include "parser.h"

Expression::Expression(std::vector<std::string>& token, std::vector<std::string>& values, std::string name, int line) 
	:token(token), values(values) {
	this->name = name;
	this->line = line;
}

Expression::~Expression() {
	operation.~queue();
}

void Expression::catchExceptionMessage(std::string message) {
	std::cout << "In line: " << line << " " << exceptionThrownValue << message << std::endl;
}

void Expression::variables() {
	try {
		if (token.size() != VARIABLE_DECLARATION_PARAM_AMOUNT) throw std::invalid_argument("Invalid variable declaration, bad amount of parameters.");
		if (token.at(0) == KEYWORD && token.at(1) == IDENTIFIER && token.at(2) == LITERAL) {
			operation.push(VARIABLE_DECLARATION);
			operation.push(values.at(0));
			operation.push(values.at(1));
			operation.push(values.at(2));
		}
		else throw std::invalid_argument("Invalid variable declaration.");
		saveOperation();
	}
	catch (std::invalid_argument& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Expected: <var type> <name> <value>, eg.dint myVar 10");
	}
}

void Expression::oneParameterStandardFunction() {
	try {
		if (token.size() != FUNCTION_ONE_PARAMETER) throw std::invalid_argument("Bad amount of parameters.");
		if (token.at(0) == KEYWORD && (token.at(1) == REGISTER || token.at(1) == IDENTIFIER)) {
			operation.push(STANDARD_FUNCTION);
			operation.push(values.at(0));
			operation.push(values.at(1));
		}
		else throw std::logic_error("Invalid argument.");
		saveOperation();
	}
	catch (std::invalid_argument& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Expected one argument");
	}
	catch (std::logic_error& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Expected: <function> <register/variable>");
	}
}

void Expression::twoParameterStandardFunction() {
	try {
		if (token.size() != FUNCTION_TWO_PARAMETERS) throw std::invalid_argument("Bad amount of parameters.");
		if (token.at(0) == KEYWORD && (token.at(1) == REGISTER || token.at(1) == IDENTIFIER) && (token.at(2) == KEYWORD || token.at(2) == LITERAL || token.at(2) == IDENTIFIER)) {
			operation.push(STANDARD_FUNCTION);
			operation.push(values.at(0));
			operation.push(values.at(1));
			operation.push(values.at(2));
		}
		else throw std::logic_error("Invalid arguments.");
		saveOperation();
	}
	catch (std::invalid_argument& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Expected two arguments.");
	}
	catch (std::logic_error& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Expected: <function> <destination>, <value>");
	}
}

void Expression::declareSections() {
	auto find = [](std::string element)->bool {
		if (std::find(sectionTypes, std::end(sectionTypes), element) != std::end(sectionTypes)) return 0;
		else return 1;
	};
	try {
		if (token.size() != SECTION_PARAM_AMOUNT) throw std::invalid_argument("Bad amount of parameters.");
		if (token.at(0) == KEYWORD && token.at(1) == LITERAL && find(token.at(1))) {
			operation.push(SECTION_DECLARATION);
			operation.push(values.at(1));
		}
		else throw std::logic_error("Unrecognised section type.");
		saveOperation();
	}
	catch (std::invalid_argument& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Expected 1 argument: section <text/data>");
	}
	catch (std::logic_error& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Expected data or text");
	}
}

void Expression::declareMainFunction() {
	try {
		if (token.size() != FUNCTION_MAIN_DECLARATION) throw std::invalid_argument("Bad amount of parameters.");
		if (token.at(0) == KEYWORD && token.at(1) == LABEL) {
			operation.push(MAIN_DECLARATION);
			operation.push(values.at(1));
		}
		else throw std::logic_error("Invalid global function name.");
		saveOperation();
	}
	catch (std::invalid_argument& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Expected 1 parameter: global <function name>():");		
	}
	catch (std::logic_error& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Expected: global <function name>():");
	}
}

void Expression::runFunction() {
	try {
		if (token.size() != 1) throw std::invalid_argument("Bad amount of parameters.");
		else operation.push(RUN);
		saveOperation();
	}
	catch (std::invalid_argument& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Run function is not taking parameters");
	}
}

void Expression::declareFunctionOrLabel() {
	try {
		if (token.size() != 1) throw std::invalid_argument("Bad amount of parameters.");
		std::string identify = values.at(0);
		if (identify[identify.length() - 3] == 40) {
			operation.push(FUNCTION_DECLARATION);
			operation.push(values.at(0));
		}
		else {
			operation.push(LABEL_DECLARATION);
			operation.push(values.at(0));
		}
		saveOperation();
	}
	catch (std::invalid_argument& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Functions and labels are not taking parameters");
	}
}

void Expression::jumpToLabel() {
	try {
		if (token.size() != 2) throw std::invalid_argument("Bad amount of parameters.");
		if (token.at(0) == KEYWORD && token.at(1) == LITERAL) {
			operation.push(JUMP);
			operation.push(values.at(0));
			operation.push(values.at(1));
		}
		else throw std::logic_error("Invalid jump area.");
		saveOperation();
	}
	catch (std::invalid_argument& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Expected 1 argument. Jump area");
	}
	catch (std::logic_error& e) {
		exceptionThrownValue = e.what();
		catchExceptionMessage(" Expected name of label or function");
	}
}

void Expression::saveOperation() {
	File* file = new File(name);
	file->saveToFile(operation);
	delete file;
}