#pragma once
#include <iostream>
#include <string>
#include <queue>
#include <vector>
#include "instructionSet.h"

class Lex {
private:
	std::string line;
	std::queue <std::string> words;
	std::vector <std::string>& token;
	std::vector <std::string>& value;
	void split();
	void match();
public:
	Lex(std::string line, std::vector<std::string>& tok, std::vector<std::string>& val);
	~Lex();
	void getLexem();
};