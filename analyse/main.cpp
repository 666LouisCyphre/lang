#include <iostream>
#include <ctime>
#include "analyze.h"
//#include <vld.h>

int main(/*int argc, char* argv[]*/void) {
#if 0
	if (argc == 2) {
		std::string name = argv[1];
		Analyze* analyze = new Analyze(name);
		analyze->run();
		std::cout << "Fine";
	}
	else {
		std::cout << "Expected argument";
		exit(EXIT_FAILURE);
	}
#endif
	clock_t start, stop;
	start = clock();
	Analyze* analyze = new Analyze("tets.txt");
	analyze->run();
	delete analyze;
	stop = clock();
	double time = (double)(stop - start) / CLOCKS_PER_SEC;
	std::cout << "\nDone in: " << time << "seconds.\nPress enter to continue . . . ";
	(void)getchar();
	(void)getchar();
	return 0;
}