#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <stdexcept>
#include <cstdlib>
#include "file.h"
#include "instructionSet.h"


#define VARIABLE_DECLARATION_PARAM_AMOUNT 3
#define FUNCTION_ONE_PARAMETER 2
#define FUNCTION_TWO_PARAMETERS 3
#define SECTION_PARAM_AMOUNT 2
#define FUNCTION_MAIN_DECLARATION 2
#define FUNCTION_DECLARATION_PARAM_AMOUNT 1


#define VARIABLE_DECLARATION "VARIABLE_DECLARATION"
#define STANDARD_FUNCTION "STANDARD_FUNCTION"
#define SYSTEM_FUNCTION "SYSTEM_FUNCTION"
#define FUNCTION_DECLARATION "FUNCTION_DECLARATION"
#define SECTION_DECLARATION "SECTION_DECLARATION"
#define LABEL_DECLARATION "LABEL_DECLARATION"
#define MAIN_DECLARATION "MAIN_DECLARATION"
#define JUMP "JUMP"
#define RUN "RUN"


class Expression {
private:
	int line;
	std::string name;
	std::queue<std::string>operation;
	std::vector<std::string>& token;
	std::vector<std::string>& values;
	std::string exceptionThrownValue;
	void catchExceptionMessage(std::string message);
public:
	Expression(std::vector<std::string>& token, std::vector<std::string>& values, std::string name, int line);
	~Expression();
	void variables();
	void oneParameterStandardFunction();
	void twoParameterStandardFunction();
	void saveOperation();
	void declareSections();
	void declareMainFunction();
	void runFunction();
	void declareFunctionOrLabel();
	void jumpToLabel();
};

class Parser {
private:
	std::vector<std::string>& token;
	std::vector<std::string>& value;
	std::string fileName;
	int line;
public:
	Parser(std::vector<std::string>& token, std::vector<std::string>& values, int line);
	~Parser();
	void parse(std::string fileName);
};