#include <iostream>
#include "file.h"

File::File(std::string name) {
	this->name = name;
}

File::~File() {
	//file1.~basic_fstream();
	name.~basic_string();
}

void File::clearFile() {
	file1.open(name, std::ios::out | std::ios::trunc);
	file1.close();
}

void File::saveToFile(std::queue<std::string>& operationList) {
	file1.open(name, std::ios::out | std::ios::app);
	int i = 0;
	int size = operationList.size();
	for (i; i < size; ++i) {
		file1 << operationList.front() << "\n";
		operationList.pop();
	}
	file1.close();
}

void File::printFile() {
	file1.open(name, std::ios::in);
	std::string line = "";
	if (!file1.good()) std::cout << "The file can not be open";
	while (getline(file1, line)) {
		std::cout << line << std::endl;
	}
	file1.close();
}

void File::readFile(std::queue<std::string>& operationList) {
	file1.open(name, std::ios::in);
	std::string line = "";
	if (!file1.good()) std::cout << "The file can not be open";
	while (getline(file1, line)) {
		operationList.push(line);
	}
	file1.close();
}

void File::readSpecificLine(int lineNumber, std::string& line) {
	file1.open(name, std::ios::in);
	int specificLine = 1;
	while (getline(file1, line)) {
		if (specificLine == lineNumber) break;
		++specificLine;
	}
	file1.close();
}