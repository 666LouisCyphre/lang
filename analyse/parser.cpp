#include <iostream>
#include "parser.h"

Parser::Parser(std::vector<std::string>& token, std::vector<std::string>& values, int line) 
	:token(token), value(values) {
	this->line = line;
}

Parser::~Parser() {
	fileName.~basic_string();
}

void Parser::parse(std::string fileName) {
	if (!token.empty()) {
		Expression* expression = new Expression(token, value, fileName, line);
		auto delObjAtExit = [expression]() {
			expression->~Expression();
			delete expression;
		};
		try {
			if (token.at(0) == LABEL) expression->declareFunctionOrLabel();
			else {
				std::string func = value.at(0);
				auto index = std::find(instructionSet, std::end(instructionSet), func);
				if (!(index != std::end(instructionSet))) throw std::logic_error("Unrecognised instruction");
				unsigned int choice = distance(instructionSet, index) + 1;
				if (choice <= 13) expression->twoParameterStandardFunction();
				else if (choice <= 21) expression->jumpToLabel();
				else if (choice == 21 || choice == 22) expression->oneParameterStandardFunction();
				else if (choice == 23) expression->declareSections();
				else if (choice == 24) expression->declareMainFunction();
				else if (choice > 24 && choice <= 27) expression->variables();
				else if (choice == 28) expression->runFunction();
				else throw std::out_of_range("Unrecognised instruction");
			}
		}
		catch (std::out_of_range & e) {
			std::cout << " Fatal error: " << e.what();
			delObjAtExit();
			(void)getchar();
			exit(EXIT_FAILURE);
		}
		catch (std::logic_error & e) {
			std::string line = "";
			int i = 0;
			int size = value.size();
			for (i; i < size; ++i) {
				line += value.at(i);
				line += " ";
			}
			std::cout << e.what() << " See line: " << line;
			delObjAtExit();
			(void)getchar();
			exit(EXIT_FAILURE);
		}
		delObjAtExit();
	}
}