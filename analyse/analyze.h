#pragma once
#include <iostream>
#include <limits>
#include "file.h"
#include "lexer.h"
#include "parser.h"

class Analyze {
private:
	std::fstream file1;
	std::vector<std::string>token;
	std::vector<std::string>value;
	std::string fileName;
	std::string outputFileName;
	std::string line;
	int lineNumber;
public:
	Analyze(std::string name);
	~Analyze();
	int fileSize();
	void toLower(std::string& str);
	void clearVectors();
	void nameOutputFile();
	void run();
};
